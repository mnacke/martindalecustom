package com.martindale.custom.server.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.martindale.custom.server.bo.base.BaseQuotationDetailMartindale;
import com.netappsid.configurator.IConfigurator;
import com.netappsid.erp.server.bo.Currency;
import com.netappsid.erp.server.bo.CurrencyExchange;
import com.netappsid.erp.server.services.beans.CurrencyServicesBean;
import com.netappsid.erp.server.services.interfaces.CurrencyServices;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.local.LoaderLocal;

public class QuotationDetailMartindale extends BaseQuotationDetailMartindale
{
	@Override
	public void putConfiguratorGlobalVariables(IConfigurator configurator)
	{
		super.putConfiguratorGlobalVariables(configurator);

		String currencyToDisplay = getSaleTransaction().getCurrency().getCode();
		configurator.putGlobalVariable("naid.currency", currencyToDisplay);

		List<CurrencyExchange> currencyExchangeList = getCurrencyExchangeList(new Date());
		if (currencyExchangeList != null && !currencyExchangeList.isEmpty())
		{
			Map<String, Map<String, Double>> conversionTable = new HashMap<String, Map<String, Double>>();
			for (CurrencyExchange currencyExchange : currencyExchangeList)
			{
				String currencyFrom = currencyExchange.getCurrencyFrom().getCode();
				String currencyTo = currencyExchange.getCurrencyTo().getCode();
				Double rate = currencyExchange.getRate().doubleValue();

				// Add the currency conversion in the map (from -> to : rate)
				Map<String, Double> exchangeRate = conversionTable.get(currencyFrom);
				if (exchangeRate == null)
				{
					exchangeRate = new HashMap<String, Double>();
				}
				exchangeRate.put(currencyTo, rate);
				conversionTable.put(currencyFrom, exchangeRate);

				// Add the reverse exchange rate only if it doesn't exist yet (BC support to create a conversion CAD to USD and another one, USD to CAD. In a
				// such case, we don't want to use the reverse of those conversion) (to -> from : reverse rate)
				Map<String, Double> reverseExchangeRate = conversionTable.get(currencyTo);
				if (reverseExchangeRate == null)
				{
					reverseExchangeRate = new HashMap<String, Double>();
				}

				if (reverseExchangeRate.get(currencyFrom) == null)
				{
					reverseExchangeRate.put(currencyFrom, 1d / rate);
					conversionTable.put(currencyTo, reverseExchangeRate);
				}
			}

			configurator.putGlobalVariable("naid.currency.conversion", conversionTable);
		}
	}

	/**
	 * @param date
	 * @return
	 */
	List<CurrencyExchange> getCurrencyExchangeList(Date date)
	{
		List<CurrencyExchange> currencyExchangeList = null;

		List<Criterion> criteriaList = new ArrayList<Criterion>();
		List<String> subcriteriaList = new ArrayList<String>();
		List<Integer> subcriteriaJoinSpecList = new ArrayList<Integer>();

		// Criterions when the currency Exchange is directly in the database
		criteriaList.add(Restrictions.eq(CurrencyExchange.PROPERTYNAME_DATE, date));
		subcriteriaList.add(null);
		subcriteriaJoinSpecList.add(null);

		ServiceLocator<Loader> loader = new ServiceLocator<Loader>(LoaderBean.class);
		currencyExchangeList = loader.get().findByCriteria(CurrencyExchange.class, CurrencyExchange.class, criteriaList, subcriteriaList,
				subcriteriaJoinSpecList);

		return (currencyExchangeList);
	}
}
